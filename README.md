# Serbot

#### 介绍
Serbot on osu!是一款基于酷Q Pro开发的一个osu!Bot,她正在处于开发阶段，欢迎任何形式的意见或者建设性的建议。

（某猫：你妈的这玩意bug真多啊）

酷Q:https://cqp.cc/

#### 部署

1.因为目录是写在源码里面的，绑死了，所以请把基础文件解压到D盘根目录。（某猫：0.4.8c可以自定义路径了）

2.安装ImageMagick。

3.将D:Profile/default/setting.ini文件的配置项[constant]，apikey=xxxx更改为你的osu!apikey。(如果没有，请到https://osu.ppy.sh/p/api 申请一个osu!apikey。（某猫：0.4.8c不需要）

4.在Profile/default/setting.ini文件中的Master项，QQ那一栏填写最高权限管理员的QQ（必填）。（某猫：0.4.8c也不需要）

5.请用osubot文件夹内的酷Q Pro来操作。（某猫：0.4.8c也不需要）

6.如果你要是觉得某些功能不好用，或者很难维护的话那请改源码吧。（前提是你真的能读懂，先抱歉了，个人能力有限，自己写的代码自己都看不懂，大草...）（某猫：是 能看懂 但是改起来真的费劲（（

7.如果你对这个东西感兴趣，你可以发送至邮箱serbot@foxmail.com留下QQ让我加你，或者加群823348411，咱们可以一起交流。（其实就是想组个小团队来完成这个东西。毕竟我才高三，太草了...）（某猫：Shatnine永远滴神！）

#### 开发

本项目遵循SATA协议开源。

首先，你得准备好你的易语言（至少5.8或更高）和易语言助手 精易模块（7.5或更高）

然后 你得很有耐心

下面是目录结构

|  文件名或目录名   | 干啥用的  |
|  ----  | ----  |
| /backup/  | 这是原始源码 没被改过的 |
| /app.e  | 这个是我（某猫）修改的0.4.3的源码 能用（除recentplay和bestplay外） |
| /v0.4.8c.e | 这个是我修改的0.4.8c的源码 用不了 修不好 未知错误（大悲）就是无法生成图片 不知道问题在哪
| /runexe.e | 这是外部执行程序的源码 没啥用
| /SozaCore/ | 测试目录 看名字就知道是小泽bot的核心目录（

其他的自己看吧（

#### 关于

1. 目前由本人以及hahanira维护。（这个fork仓库由某猫维护）
2. 如果喜欢 或者欣赏我们，不如赞助我们！（某猫：您的发电链接呢（ 反正我的[在这](https://afdian.net/@catnetwork)
3. 最终解释权归Shatnine所有。（某猫：确实 有bug事他的锅（（

#### 展示
![](https://images.gitee.com/uploads/images/2019/1212/213633_fad49cb5_2041302.png "个人资料")
![](https://images.gitee.com/uploads/images/2019/1212/213542_6ea9d293_2041302.jpeg "记录回放")

#### 参与贡献

1. Sayobot(小夜）
2. hahanira（老老阔）
3. SozaBot（小泽）